JournalApp.Views.PostsIndex = Backbone.View.extend({

  template: JST['posts/index'],
  
  initialize: function() {
    this.posts = JournalApp.Collections.posts;
    this.listenTo(this.posts, "remove add change:title reset", this.render);
  },
  
  
  deletePost: function(event) {
    var id = parseInt($(event.currentTarget).attr("data-id"));
    var post = this.posts.get(id);
    console.log("sith");
    post.destroy();
    this.posts.fetch();
  },
  
  events: {
    "click .delete-button": "deletePost", 
  },
  
  render: function() {
    this.$el.html(this.template({ collection: this.posts }));
    return this;
  }

});
