JournalApp.Views.PostForm = Backbone.View.extend({
  
  template: JST["post/form"],
  
  initialize: function() {
    
  },
  
  render: function() {
    var renderedContent = this.template({ post: this.model });
    this.$el.html(renderedContent);
    return this;
  },
  
  events: {
    "post-form submit", "addPost"
  }
  
  addPost: function() {
    alert("it worked");
  }
  
});