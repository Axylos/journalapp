window.JournalApp = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    
    JournalApp.Collections.posts.fetch({ success: function() { 
      var routes = new JournalApp.Routers.Posts()
      Backbone.history.start();
    }
  });
    
    // var posts = new JournalApp.Collections.Posts()
  }
};

$(document).ready(function(){
  JournalApp.initialize();
});
