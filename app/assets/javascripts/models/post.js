JournalApp.Models.Post = Backbone.Model.extend({
  urlRoot: '/api/posts',
  collection: JournalApp.Collections.Posts
});
