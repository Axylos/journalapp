JournalApp.Routers.Posts = Backbone.Router.extend({
  routes: {
    '': 'index',
    'posts/:id': 'show',
    'posts/new': 'new'
  },
  
  initialize: function() {
    this.index();
  },
  
  new: function(post) {
    var newPost = new JournalApp.Models.Post();
    var newView = new JournalApp.Views.PostForm({
      model: newPost
    });
    $("body").html(newView.render().$el);
  },
  
  index: function () {
    var indexView = new JournalApp.Views.PostsIndex();
    $("body").html(indexView.render().$el);
  },
  
  show: function(id) {
    var post = JournalApp.Collections.posts.get(id);
    var showView = new JournalApp.Views.PostShow({
      model: post
    });
    $("body").html(showView.render().$el);
  }
});
