Rails.application.routes.draw do
  root to: 'roots#root'
  
  namespace :api do
    resources :posts, only: [:create, :index, :destroy, :show]
  end
end
